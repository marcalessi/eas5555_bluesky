# Week 1 Script to Analyze WRF output

import netCDF4 as nc
import numpy as np
from mpl_toolkits.basemap import Basemap as bm
import matplotlib.pyplot as plt
from matplotlib import cm
import matplotlib as mpl

ncfile = nc.Dataset('/home/mja244/eas5555/wrfout.nc')

lat = np.asarray(ncfile.variables['XLAT'])
lon = np.asarray(ncfile.variables['XLONG'])

print lat.shape
print lon.shape
